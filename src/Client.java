import java.io.*;
import java.net.Socket;


class Client implements Runnable {
    private BufferedReader in;
    private PrintWriter out;
    private boolean done;
    private Socket client;

    public static void main(String[] args) {
        Client  c = new Client();
        c.run();
    }

    @Override
    public void run() {
        try {
            client = new Socket("127.0.0.1", 9999);
            out = new PrintWriter(client.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(client.getInputStream()));

            InputHandler inHandler = new InputHandler();
            Thread niki = new Thread(inHandler);
            niki.start();

            String inmessage;
            while ((inmessage = in.readLine()) != null) {
                System.out.println(inmessage);
            }

        } catch (IOException e) {
            shutdown();
        }
    }

    public void shutdown() {
        done = true;
        try {
            in.close();
            out.close();
            if (!client.isClosed()) {
                client.close();
            }
        } catch (IOException e) {

        }

    }


    class InputHandler implements Runnable {
        @Override
        public void run() {
            try {
                BufferedReader inReader = new BufferedReader(new InputStreamReader(System.in));
                while (!done) {
                    String mes = inReader.readLine();
                    if (mes.equals("/quit")) {
                        out.println(mes);
                        inReader.close();
                        shutdown();
                    } else {
                        out.println(mes);
                    }
                }
            } catch (IOException e) {
                shutdown();
            }
        }
    }


}