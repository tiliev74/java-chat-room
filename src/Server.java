import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server implements Runnable {
    private ArrayList<ConnectionHandler> connections;
    private ServerSocket server;
    private boolean done;
    private ExecutorService pool;

    public Server() {
        connections = new ArrayList<>();
        done = false;
    }


    @Override
    public void run() {
        try {
            server = new ServerSocket(9999);
            pool = Executors.newCachedThreadPool();
            while (!done) {
                Socket client = server.accept();
                ConnectionHandler handler = new ConnectionHandler(client);
                connections.add(handler);
                pool.execute(handler);
            }

        } catch (Exception e) {
            shutdown();
        }
    }

    public void broadcast(String message) {
        for (ConnectionHandler ch : connections) {
            if (ch != null) {
                ch.sendMessage(message);
            }
        }
    }


    public void shutdown() {
        done = true;
        if (!server.isClosed()) {
            try {
                server.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            for (ConnectionHandler ch : connections) {
                try {
                    ch.shutdown();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }

    }


    class ConnectionHandler implements Runnable {
        private Socket client;
        private BufferedReader in;
        private PrintWriter out;
        private String nickname;


        public ConnectionHandler(Socket client) {
            this.client = client;
        }

        @Override
        public void run() {
            try {
                out = new PrintWriter(client.getOutputStream(), true);
                in = new BufferedReader(new InputStreamReader(client.getInputStream()));
                out.println("Hello give nickname ");
                nickname = in.readLine();
                System.out.println(nickname + " connected!");
                broadcast(nickname + " has joined!");
                String message;
                while ((message = in.readLine()) != null) {
                    if (message.startsWith("/nick ")) {
                        String[] messagesplit = message.split(" ", 2);
                        if (messagesplit.length == 2) {
                            broadcast(nickname + " renamed themselves to " + messagesplit[1]);
                            System.out.println(nickname + " renamed themselves to " + messagesplit[1]);
                            nickname = messagesplit[1];
                            out.println("Successfully changed his name to " + nickname);
                        } else {
                            out.println("No nickname was provided ");
                        }
                    } else if (message.startsWith("/quit")) {
                        broadcast(nickname + " left the chat!");
                        shutdown();
                    } else {
                        broadcast(nickname + ": " + message);
                    }
                }


            } catch (IOException e) {
                try {
                    shutdown();
                } catch (IOException ex) {
                    throw new RuntimeException(ex);

                }
            }
        }

        public void sendMessage(String niki) {
            out.println(niki);
        }

        public void shutdown() throws IOException {
            in.close();
            out.close();
            if (!client.isClosed()) {
                client.close();

            }
        }

    }

    public static void main(String[] args) {
        Server server = new Server();
        server.run();
    }

}
